/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_UART_Ring.c
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#include "uart/re_app_UART_Ring.h"

static uint8_t ble_rcvd_char_count = 0;
uint8_t ble_tx_buffer[16];
uint16_t LenOfble_tx_buffer;
volatile bool uart1RxCmplt = false;
volatile bool uart2RxCmplt = false;
uint32_t Initial_Odo;
uint8_t ble_data_packets_rcvd;

ring_buffer_uart1 rx_buffer_uart1 = {{0}, 0, 0};
ring_buffer_uart1 tx_buffer_uart1 = {{0}, 0, 0};
ring_buffer_uart2 rx_buffer_uart2 = {{0}, 0, 0};
ring_buffer_uart2 tx_buffer_uart2 = {{0}, 0, 0};
ring_buffer_uart1 *_rx_buffer_uart1;
ring_buffer_uart1 *_tx_buffer_uart1;
ring_buffer_uart2 *_rx_buffer_uart2;
ring_buffer_uart2 *_tx_buffer_uart2;

/**
  * @Brief Ringbuf_Init
  * This function initialises the ring buffer and also enables interrupts
  * @Param None
  * @Retval None
  */
void Ringbuf_Init(void)
{
    _rx_buffer_uart1 = &rx_buffer_uart1;
    _tx_buffer_uart1 = &tx_buffer_uart1;
    _rx_buffer_uart2 = &rx_buffer_uart2;
    _tx_buffer_uart2 = &tx_buffer_uart2;   
    /* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
    __HAL_UART_ENABLE_IT(&huart1_t, UART_IT_ERR);
    /* Enable the UART Data Register not empty Interrupt */
    __HAL_UART_ENABLE_IT(&huart1_t, UART_IT_RXNE);
    /* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
    __HAL_UART_ENABLE_IT(&huart2_t, UART_IT_ERR);
    /* Enable the UART Data Register not empty Interrupt */
    __HAL_UART_ENABLE_IT(&huart2_t, UART_IT_RXNE);    
}

/**
  * @Brief Ringbuf_UART1_Free
  * This function frees the ring buffer of UART1
  * @Param None
  * @Retval None
  */
void Ringbuf_UART1_Free(void)
{
    memset(_rx_buffer_uart1->buffer_uart1, 0x00, UART1_BUFFER_SIZE);
    _rx_buffer_uart1->head_uart1 = NULL;
    _rx_buffer_uart1->tail_uart1 = NULL;
    memset(_tx_buffer_uart1->buffer_uart1, 0x00, UART1_BUFFER_SIZE);
    _tx_buffer_uart1->head_uart1 = NULL;
    _tx_buffer_uart1->tail_uart1 = NULL;
}

/**
  * @Brief Ringbuf_UART2_Free
  * This function frees the ring buffer of UART2
  * @Param None
  * @Retval None
  */
void Ringbuf_UART2_Free(void)
{
    memset(_rx_buffer_uart2->buffer_uart2, 0x00, UART2_BUFFER_SIZE);
    _rx_buffer_uart2->head_uart2 = NULL;
    _rx_buffer_uart2->tail_uart2 = NULL;
    memset(_tx_buffer_uart2->buffer_uart2, 0x00, UART2_BUFFER_SIZE);
    _tx_buffer_uart2->head_uart2 = NULL;
    _tx_buffer_uart2->tail_uart2 = NULL;
}

/**
  * @Brief Store_UART1_Char
  * This function stores the received charcter from UART1 into UART1 ring buffer
  * @Param char 
  * @Param pointer to ting buffer
  * @Retval None
  */
void Store_UART1_Char(unsigned char c, ring_buffer_uart1 *buffer)
{
    uint32_t i = (uint32_t)(buffer->head_uart1 + 1) % UART1_BUFFER_SIZE;
   /** 
   * if we should be storing the received character into the location
   * just before the tail (meaning that the head would advance to the
   * current location of the tail), we're about to overflow the buffer
   * and so we don't write the character or advance the head.
   */
    if (i != buffer->tail_uart1)
    {
        buffer->buffer_uart1[buffer->head_uart1] = c;
        buffer->head_uart1 = i;
    }
}

/**
  * @Brief Store_UART2_Char
  * This function stores the received charcter from UART2 into UART2 ring buffer
  * @Param char 
  * @Param pointer to ting buffer
  * @Retval None
  */
void Store_UART2_Char(unsigned char c, ring_buffer_uart2 *buffer)
{
    uint32_t i = (uint32_t)(buffer->head_uart2 + 1) % UART2_BUFFER_SIZE;
   /** 
   * if we should be storing the received character into the location
   * just before the tail (meaning that the head would advance to the
   * current location of the tail), we're about to overflow the buffer
   * and so we don't write the character or advance the head.
   */
    if (i != buffer->tail_uart2)
    {
        buffer->buffer_uart2[buffer->head_uart2] = c;
        buffer->head_uart2 = i;
    }
}

#if 0
static int UART2_Read(void)
{
    // if the head isn't ahead of the tail, we don't have any characters
    if (_rx_buffer_uart2->head_uart2 == _rx_buffer_uart2->tail_uart2)
    {
        return -1;
    }
    else
    {
        unsigned char c = _rx_buffer_uart2->buffer_uart2[_rx_buffer_uart2->tail_uart2];
        _rx_buffer_uart2->tail_uart2 = (unsigned int)(_rx_buffer_uart2->tail_uart2 + 1) % UART_BUFFER_SIZE;
        return c;
    }
}


static int UART3_Read(void)
{
    // if the head isn't ahead of the tail, we don't have any characters
    if (_rx_buffer_uart3->head_uart3 == _rx_buffer_uart3->tail_uart3)
    {   
        return -1;
    }
    else
    {
        unsigned char c = _rx_buffer_uart3->buffer_uart3[_rx_buffer_uart3->tail_uart3];
        _rx_buffer_uart3->tail_uart3 = (unsigned int)(_rx_buffer_uart3->tail_uart3 + 1) % UART_BUFFER_SIZE;
        return c;
    }
}


//make this better
void Get_UART2_String(char *buffer)
{
    int index = 0;
    unsigned int start = _rx_buffer_uart2->tail_uart2;
    unsigned int end = (_rx_buffer_uart2->head_uart2);
    if ((_rx_buffer_uart2->tail_uart2 > _rx_buffer_uart2->head_uart2) && (_rx_buffer_uart2->buffer_uart2[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < UART_BUFFER_SIZE; i++)
        {
            buffer[index] = UART2_Read();
            index++;
        }
        for (unsigned int i = 0; i < end; i++)
        {
            buffer[index] = UART2_Read();
            index++;
        }
    }
    else if ((_rx_buffer_uart2->buffer_uart2[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < end; i++)
        {
                buffer[index] = UART2_Read();
                index++;
        }
    }
}


void Get_UART3_String(char *buffer)
{
    int index = 0;
    unsigned int start = _rx_buffer_uart3->tail_uart3;
    unsigned int end = (_rx_buffer_uart3->head_uart3);
    if ((_rx_buffer_uart3->tail_uart3 > _rx_buffer_uart3->head_uart3) && (_rx_buffer_uart3->buffer_uart3[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < UART_BUFFER_SIZE; i++)
        {
            buffer[index] = UART3_Read();
            index++;
        }
        for (unsigned int i = 0; i < end; i++)
        {
                buffer[index] = UART3_Read();
                index++;
        }
    }
    else if ((_rx_buffer_uart3->buffer_uart3[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < end; i++)
        {
                buffer[index] = UART3_Read();
                index++;
        }
    }
}
#endif

/**
  * @Brief This function handles UART global interrupt.
  */
void UART_ISR(UART_HandleTypeDef *huart)
{
    if(huart -> Instance == USART1)
    {
        uint32_t isrflags = READ_REG(huart->Instance->SR);
        uint32_t cr1its   = READ_REG(huart->Instance->CR1);
        /* if DR is not empty and the Rx Int is enabled */
        if (((isrflags & USART_SR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET))
        {
            /******************
            * @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
            *         error) and IDLE (Idle line detected) flags are cleared by software
            *         sequence: a read operation to USART_SR register followed by a read
            *         operation to USART_DR register.
            * @note   RXNE flag can be also cleared by a read to the USART_DR register.
            * @note   TC flag can be also cleared by software sequence: a read operation to
            *         USART_SR register followed by a write operation to USART_DR register.
            * @note   TXE flag is cleared only by a write to the USART_DR register.
            *********************/
            huart->Instance->SR;		            // Read status register 
            unsigned char c = huart->Instance->DR;          // Read data register 
            Store_UART1_Char(c, _rx_buffer_uart1);          // store data in buffer
            /** if'c' == '\n'*/
            if ('\n' == c)
            {
                uart1RxCmplt = true;
            }
            return;
        }
        /*If interrupt is caused due to Transmit Data Register Empty */
        if (((isrflags & USART_SR_TXE) != RESET) && ((cr1its & USART_CR1_TXEIE) != RESET))
        {
            if (tx_buffer_uart1.head_uart1 == tx_buffer_uart1.tail_uart1)
            {
                // Buffer empty, so disable interrupts
                __HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
            }
            else
            {
                // There is more data in the output buffer. Send the next byte
                unsigned char c = tx_buffer_uart1.buffer_uart1[tx_buffer_uart1.tail_uart1];
                tx_buffer_uart1.tail_uart1 = (tx_buffer_uart1.tail_uart1 + 1) % UART1_BUFFER_SIZE;
                /******************
                * @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
                *         error) and IDLE (Idle line detected) flags are cleared by software
                *         sequence: a read operation to USART_SR register followed by a read
                *         operation to USART_DR register.
                * @note   RXNE flag can be also cleared by a read to the USART_DR register.
                * @note   TC flag can be also cleared by software sequence: a read operation to
                *         USART_SR register followed by a write operation to USART_DR register.
                * @note   TXE flag is cleared only by a write to the USART_DR register.
                *********************/
                huart->Instance->SR;
                huart->Instance->DR = c;
            }
            return;
        }
    }
    else if(huart -> Instance == USART2)
    {
        uint32_t isrflags = READ_REG(huart->Instance->SR);
        uint32_t cr1its   = READ_REG(huart->Instance->CR1);
        /* if DR is not empty and the Rx Int is enabled */
        if (((isrflags & USART_SR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET))
        {
            /******************
            * @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
            *         error) and IDLE (Idle line detected) flags are cleared by software
            *         sequence: a read operation to USART_SR register followed by a read
            *         operation to USART_DR register.
            * @note   RXNE flag can be also cleared by a read to the USART_DR register.
            * @note   TC flag can be also cleared by software sequence: a read operation to
            *         USART_SR register followed by a write operation to USART_DR register.
            * @note   TXE flag is cleared only by a write to the USART_DR register.
            *********************/
            huart->Instance->SR;				   // Read status register 
            unsigned char c = huart->Instance->DR;                 // Read data register 
            ble_rcvd_char_count++;
            Store_UART2_Char(c, _rx_buffer_uart2);		   // store data in buffer
                        /** if'c' == '\n'*/
            if ('#' == c || ble_rcvd_char_count == 16)
            {
                ble_data_packets_rcvd++;
                if(ble_data_packets_rcvd == 8)
                {
                  __NOP();
                }
                ble_rcvd_char_count = 0;
                // Increment counter for every packet received.
                uart2RxCmplt = true;
                //1.  Push data into FIFO. FIFO_Len = 4 x rx_buff
                //2.  Clear rx_buffer
            }
            return;
        }
        /*If interrupt is caused due to Transmit Data Register Empty */
        if (((isrflags & USART_SR_TXE) != RESET) && ((cr1its & USART_CR1_TXEIE) != RESET))
        {
            if (tx_buffer_uart2.head_uart2 == tx_buffer_uart2.tail_uart2)
            {
                // Buffer empty, so disable interrupts
                __HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
            }
            else
            {
                // There is more data in the output buffer. Send the next byte
                unsigned char c = tx_buffer_uart2.buffer_uart2[tx_buffer_uart2.tail_uart2];
                tx_buffer_uart2.tail_uart2 = (tx_buffer_uart2.tail_uart2 + 1) % UART2_BUFFER_SIZE;
                /******************
                * @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
                *         error) and IDLE (Idle line detected) flags are cleared by software
                *         sequence: a read operation to USART_SR register followed by a read
                *         operation to USART_DR register.
                * @note   RXNE flag can be also cleared by a read to the USART_DR register.
                * @note   TC flag can be also cleared by software sequence: a read operation to
                *         USART_SR register followed by a write operation to USART_DR register.
                * @note   TXE flag is cleared only by a write to the USART_DR register.
                *********************/
                huart->Instance->SR;
                huart->Instance->DR = c;
            }
            return;
        }
    }
}

/**
  * @Brief UART1_HandleData
  * This function handles the data in UART1 Ring buffer
  * @Param None
  * @Retval None
  */
void UART1_HandleData(void) 
{   
    uint8_t *month;
    uint8_t msgId = atoi(strtok((char *)rx_buffer_uart1.buffer_uart1, ","));
    switch(msgId)
    {
        case 10:   /* TimeStamp */
            sDate_t.Date      = atoi(strtok(NULL, ","));
            month             = (uint8_t *)strtok(NULL, ",");
            sDate_t.Month     = strtol((char *)month, NULL, 16);
            sDate_t.Year      = atoi(strtok(NULL, ","));
            if (HAL_RTC_SetDate(&hRTC_t, &sDate_t, RTC_FORMAT_BIN) != HAL_OK)
            {
                RE_Error_Handler(__FILE__, __LINE__);
            } 
            sTime_t.Hours     = atoi(strtok(NULL, ","));
            sTime_t.Minutes   = atoi(strtok(NULL, ","));  
            sTime_t.Seconds   = atoi(strtok(NULL, ","));
            if (HAL_RTC_SetTime(&hRTC_t, &sTime_t, RTC_FORMAT_BIN) != HAL_OK)
            {
                RE_Error_Handler(__FILE__, __LINE__);
            }
            WaitForEC20Timestamp = false;
            break;
        case 11:   /* LCD data */
            ec20_lcd_data_rx = true;
            ec20_lcd_data_rx_fail_cnt = 0;
            LcdDisplay.bat_volt         = atoi(strtok(NULL, ","));
            LcdDisplay.bat_soc          = atoi(strtok(NULL, ","));
            LcdDisplay.bat_temp         = atoi(strtok(NULL, ","));  
            LcdDisplay.efficiency       = atoi(strtok(NULL, ","));        
            break;
        case 15:
            if(atoi((char *)&rx_buffer_uart1.buffer_uart1[3]) == 1)
            {
                ec20_bat_id_ack = true;
            }
            else
            {
                RE_Tx_Bat_ID_EC20();
            }
            break;
        case 18:
            for(uint8_t i = 0; i <= 3; i++)
            {
                strcpy((char *)FinalSwapInfo[i].pid, strtok(NULL, ","));
                FinalSwapInfo[i].pid[7] = '\0';
                FinalSwapInfo[i].Energy = atoi(strtok(NULL, ","));
                FinalSwapInfo[i].Alert = atoi(strtok(NULL, ","));        
            }
            RE_Read_Struct_From_NVS(0, 0); // Memory address of NVS and battery number as i/p
            RE_Read_Struct_From_NVS(11, 1);
            RE_Read_Struct_From_NVS(32, 2);
            RE_Read_Struct_From_NVS(42, 3);
            RE_Read_DwordFromNVS(53);  //memaddr
            SwapModeFlag = true;
            memset(ble_tx_buffer, 0, sizeof(ble_tx_buffer));
            sprintf((char *)ble_tx_buffer, "%s%d,%x\n", "10",1, LcdDisplay.odo - 892);
            LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
            HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer, 100);
            break;
        default:
            break;
    }
    Ringbuf_UART1_Free();
}

/**
  * @Brief UART2_HandleData
  * This function handles the data in UART2 Ring buffer
  * @Param None
  * @Retval None
  */
void UART2_HandleData(void)
{
    uint8_t batt_numm = 0;
    uint8_t latch_cmd; 
    volatile uint8_t id_buffer[2];
    volatile uint8_t msg_id = 0;
    uint8_t unlock_latch_cmd[15] = { 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D,\
                                    0x1E, 0x1F};
    uint8_t mobile_ctrl[15];
    memcpy(mobile_ctrl, rx_buffer_uart2.buffer_uart2, 15);
    memset(ble_tx_buffer, 0, sizeof(ble_tx_buffer));
    for(uint8_t i = 0; i<=1; i++)
    {
        id_buffer[i] = rx_buffer_uart2.buffer_uart2[i];
    }
    msg_id = strtol((char *)id_buffer, NULL, 10);
    if(0 == memcmp(mobile_ctrl, unlock_latch_cmd, sizeof(unlock_latch_cmd)))
    {
        RE_Latch_Open();
//        RE_Tx_Key_Ctrl_Cmd(0); /** 0: OFF; 1:ON */
    }
    else
    {
        switch(msg_id)
        {
            case 10:
                if(atoi((char *)&rx_buffer_uart2.buffer_uart2[2]) == 1)
                {
                    packsIdVerified = false;    
                    G2G = false;
                    if(!key_status)
                    {
                        Req_SwapData_From_EC20_Flag = true;
                    }
                    else
                    {
                        sprintf((char *)ble_tx_buffer, "%s%d\n", "10",2);
                        LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
                        HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer, 100);                        
                    }
                }
                else
                {
                    sprintf((char *)ble_tx_buffer, "%s%d\n", "10",0);
                    LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
                    HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer, 100);
                }  
                Ringbuf_UART2_Free();
                break;
            case 11: 
                if(SwapModeFlag == true)
                {
                    batt_numm = (uint8_t)atoi((char *)&rx_buffer_uart2.buffer_uart2[2]);
                    batt_numm = batt_numm - 1;
                    sprintf((char *)ble_tx_buffer, "%s%d%s%04x%d\n", "11", batt_numm + 1, FinalSwapInfo[batt_numm].pid,\
                             InitialSwapInfo[batt_numm].Energy - FinalSwapInfo[batt_numm].Energy,\
                             FinalSwapInfo[batt_numm].Alert);
                    LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
                    HAL_Delay(50);
                    HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer, 100);
                    memset(ble_tx_buffer, 0, sizeof(ble_tx_buffer));
                }
                Ringbuf_UART2_Free();
                break;
            case 12:
                latch_cmd = atoi((char *)&rx_buffer_uart2.buffer_uart2[2]);
                if(SwapModeFlag == true && latch_cmd == 1)
                {
                    RE_Latch_Open();
                }
                else if(SwapModeFlag == true && latch_cmd == 2)
                {
                    sprintf((char *)ble_tx_buffer, "%s%d\n", "12", LcdDisplay.lock_status);
                    LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
                    HAL_UART_Transmit(&huart3_t, ble_tx_buffer, LenOfble_tx_buffer, 100);                
                }
                Ringbuf_UART2_Free();
                break;
            case 13:
                batt_numm = atoi((char *)&rx_buffer_uart2.buffer_uart2[2]);
                if(batt_numm == 4)
                {
                    WriteData_To_Nvs_Flag = true;
                }
                batt_numm = batt_numm - 1;
                for(uint8_t i = 0; i<=7; i++)
                {
                    InitialSwapInfo[batt_numm].pid[i] = rx_buffer_uart2.buffer_uart2[i+3];
                }
                InitialSwapInfo[batt_numm].pid[7] = '\0'; 
                char AvailEnergy_buffer[5];
                for(uint8_t i = 0; i<=3; i++)
                {
                    AvailEnergy_buffer[i] = rx_buffer_uart2.buffer_uart2[i+10];
                }
                InitialSwapInfo[batt_numm].Energy = (uint16_t)strtol((char *)AvailEnergy_buffer, (char **)&AvailEnergy_buffer[3], 16);
                InitialSwapInfo[batt_numm].Alert = atoi((char *)&rx_buffer_uart2.buffer_uart2[14]);
                batt_numm = batt_numm + 1;
                sprintf((char *)ble_tx_buffer, "%s%d\n", "13", batt_numm);
                LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
//                HAL_Delay(100);
                HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer,100);
                Ringbuf_UART2_Free();
                break;
            case 15:
                if(atoi((char *)&rx_buffer_uart2.buffer_uart2[2]) == 1)
                {
                    G2G = true;
                    sprintf((char *)ble_tx_buffer, "%s%d\n", "15",1);
                    LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
                    HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer, 100);   
                    write_g2g_to_nvs_flag = true;
                }
                Ringbuf_UART2_Free();
                break;
            default:
                break;
        }
    }   
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/