/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_nvs.c
  * Origin Date           :   06/10/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, OCT 2020
  *****************************************************************************
  */

/* Includes */
#include "nvs/re_app_nvs.h"

#define EEPROM_ADDRESS 0xA0
uint8_t SysConfig_Buffer[sizeof(InitialSwapInfo)];

RE_StatusTypeDef RE_Write_Struct_To_NVS(uint16_t MemAddr, uint8_t bat_num)
{
    HAL_Delay(100);
    if(HAL_I2C_Mem_Write(&hi2c1_t, EEPROM_ADDRESS,  MemAddr, 0xFFFF, (uint8_t*)(&InitialSwapInfo[bat_num]),\
                            sizeof(InitialSwapInfo[bat_num]), 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

RE_StatusTypeDef RE_Read_Struct_From_NVS(uint16_t MemAddr, uint8_t bat_num)
{
    HAL_Delay(100);
    if(HAL_I2C_Mem_Read(&hi2c1_t, EEPROM_ADDRESS, MemAddr, 0xFFFF, SysConfig_Buffer, sizeof(InitialSwapInfo), 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    for(uint8_t i = 0; i <= 7; i++)
    {
        InitialSwapInfo[bat_num].pid[i] = SysConfig_Buffer[i];
    }
    InitialSwapInfo[bat_num].Energy = ((SysConfig_Buffer[9] << 8) | SysConfig_Buffer[8]);
    InitialSwapInfo[bat_num].Alert  = SysConfig_Buffer[10];
    return RE_OK;
}

RE_StatusTypeDef RE_WriteBytetoNVS(uint16_t MemAddr, uint8_t Data)
{
    if(HAL_I2C_Mem_Write(&hi2c1_t, EEPROM_ADDRESS,  MemAddr, 0xFFFF, &Data, 1, 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

RE_StatusTypeDef RE_ReadByteFromNVS(uint16_t MemAddr)
{
    uint8_t Data = 0;
    if(HAL_I2C_Mem_Write(&hi2c1_t, EEPROM_ADDRESS,  MemAddr, 0xFFFF, &Data, 1, 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    
    return RE_OK;
}

RE_StatusTypeDef RE_Write_DwordToNVS(uint16_t MemAddr, uint32_t Data)
{
    uint8_t Data_Buffer[4];
    Data_Buffer[0] =   ((Data >> 24) & 0xFF);
    Data_Buffer[1] =  ((Data >> 16) & 0xFF);
    Data_Buffer[2] = ((Data >> 8) & 0xFF);
    Data_Buffer[3] = (Data & 0xFF);
    if(HAL_I2C_Mem_Write(&hi2c1_t, EEPROM_ADDRESS,  0, 0xFFFF, Data_Buffer, 4, 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;   
}

RE_StatusTypeDef RE_Read_DwordFromNVS(uint16_t MemAddr)
{
    HAL_Delay(100);
    uint8_t Data_Buffer[4];
    if(HAL_I2C_Mem_Read(&hi2c1_t, EEPROM_ADDRESS, MemAddr, 0xFFFF, Data_Buffer, 4, 1000) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    } 
    Initial_Odo = (Data_Buffer[3]) | ((Data_Buffer[2]) << 8) | ((Data_Buffer[1]) << 16) | ((Data_Buffer[0]) << 24);
    return RE_OK;   
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/