/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_can.c
  * Origin Date           :   17/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */
/* Includes*/

#include "can/re_app_can.h"

#define CAPTURE_INFO_CMD    0x5E
#define GET_CAPTURED_INFO   0x5F
#define GET_BAT_ID_CMD      0x09
#define DIS_FET_CTRL        0x80

/** FRIGG COMMANDS */
#define KEY_CTRL_CMD             0x11
#define LATCH_CTRL_CMD           0x12
#define LATCH_STATUS_CMD         0x13
#define REV_LIGHT_CTRL_CMD       0x14  
#define SYSTEM_START_CMD         0x15
#define G2G_STATUS_CMD           0x16

BatInfo_t BatInfo[4];


/**
  * @Brief RE_Capture_Data
  * This function request the battery pack to get the data from soc
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_Capture_Data(void)
{
    uint32_t TxMailbox;
    uint8_t tx_msg;
    CAN1_TxHeader_t.DLC   = 1;
    CAN1_TxHeader_t.ExtId = 0xC9;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA; 
    tx_msg                = CAPTURE_INFO_CMD;
    if(HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, &tx_msg,  &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
  * @Brief RE_Req_CapturedData
  * This function request the battery pack for soc data
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_Req_CapturedData(uint32_t batId)
{
    uint32_t TxMailbox;
    uint8_t tx_msg[8];
    tx_msg[0]             = GET_CAPTURED_INFO;
    tx_msg[1]             = (batId & 0xFF);
    tx_msg[2]             = ((batId >> 8)  & 0xFF);
    tx_msg[3]             = ((batId >> 16) & 0xFF);
    tx_msg[4]             = ((batId >> 24) & 0xFF);
    CAN1_TxHeader_t.DLC   = 5;
    CAN1_TxHeader_t.ExtId = 0xC9;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, tx_msg, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
  * @Brief RE_Req_BatIds
  * This function request the battery pack for CANID and PhyscialID
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_Req_BatIds(void)
{
    uint32_t TxMailbox;
    uint8_t tx_message;
    CAN1_TxHeader_t.DLC   = 1;
    CAN1_TxHeader_t.ExtId = 0xC9;
    tx_message            = GET_BAT_ID_CMD;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    battery_num = 0;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, &tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
  * @Brief RE_ReqPack_TurnOFF_DischargeFET
  * This function request Rear MCU to turn OFF the Discharge FET
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_ReqPack_TurnOFF_DischargeFET(void)
{
    uint32_t TxMailbox;
    uint8_t tx_message[2];
    tx_message[0]         = DIS_FET_CTRL;
    tx_message[1]         = 0;
    CAN1_TxHeader_t.DLC   = 2;
    CAN1_TxHeader_t.ExtId = 0xCB;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
  * @Brief RE_ReqPack_TurnON_DischargeFET
  * This function request Rear MCU to turn OFF the Discharge FET
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_ReqPack_TurnON_DischargeFET(void)
{
    uint32_t TxMailbox;
    uint8_t tx_message[2];
    tx_message[0]         = DIS_FET_CTRL;
    tx_message[1]         = 1;
    CAN1_TxHeader_t.DLC   = 2;
    CAN1_TxHeader_t.ExtId = 0xCB;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
  * @Brief RE_Tx_Key_Status
  * This function command Rear MCU to turn OFF the key relay
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_Tx_Key_Ctrl_Cmd(uint8_t key_ctrl_status)
{
    uint32_t TxMailbox;
    uint8_t tx_message[2];
    tx_message[0]         = KEY_CTRL_CMD;
    tx_message[1]         = key_ctrl_status;  /** 0: OFF;  1: ON */
    CAN1_TxHeader_t.DLC   = 2;
    CAN1_TxHeader_t.ExtId = 0xCB;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}


/**
  * @Brief RE_Latch_Open
  * This function request Rear MCU to open dock latch
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_Latch_Open(void)
{
    uint32_t TxMailbox;
    uint8_t tx_message;
    tx_message            = LATCH_CTRL_CMD;
    CAN1_TxHeader_t.DLC   = 1;
    CAN1_TxHeader_t.ExtId = 0xCB;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, &tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;   
}

/**
  * @Brief RE_Get_Latch_Status
  * This function request Rear MCU for dock latch status
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_Get_Latch_Status(void)
{
    uint32_t TxMailbox;
    uint8_t tx_message;
    tx_message            = LATCH_STATUS_CMD;
    CAN1_TxHeader_t.DLC   = 1;
    CAN1_TxHeader_t.ExtId = 0xCB;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, &tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;   
}

/**
  * @Brief RE_Send_System_Start_Cmd
  * This function request Rear MCU for dock latch status
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_Rev_Light_Cmd(uint8_t rev_light_status)
{
    uint32_t TxMailbox;
    uint8_t tx_message[2];
    tx_message[0]         = REV_LIGHT_CTRL_CMD;
    tx_message[1]         = rev_light_status;
    CAN1_TxHeader_t.DLC   = 2;
    CAN1_TxHeader_t.ExtId = 0xCB;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;   
}

/**
  * @Brief RE_Send_System_Start_Cmd
  * This function request Rear MCU for dock latch status
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_Send_System_Start_Cmd(void)
{
    uint32_t TxMailbox;
    uint8_t tx_message;
    tx_message            = SYSTEM_START_CMD;
    CAN1_TxHeader_t.DLC   = 1;
    CAN1_TxHeader_t.ExtId = 0xCB;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, &tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;   
}


RE_StatusTypeDef RE_Tx_G2G_Status(uint8_t G2G_status)
{
    uint32_t TxMailbox;
    uint8_t tx_message[2];
    tx_message[0]         = G2G_STATUS_CMD;
    tx_message[1]         = G2G_status;
    CAN1_TxHeader_t.DLC   = 2;
    CAN1_TxHeader_t.ExtId = 0xCB;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/