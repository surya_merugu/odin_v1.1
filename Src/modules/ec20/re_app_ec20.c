/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_ec20.c
  * Origin Date           :   17/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "ec20/re_app_ec20.h"

uint8_t EC20_Req_Timestamp_Counter = 0;
bool WaitForEC20Timestamp = true;
bool EnableEC20 = true;
bool Tx_IdsToEc20_Flag;
bool Req_SwapData_From_EC20_Flag;


/**
  * @Brief RE_ReqEC20_Timestamp
  * This function requests the time stamp from EC20
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_ReqEC20_Timestamp(void)
{
    uint8_t ReqTimestamp[] = "*\n";
    uint16_t LenofReqTimestamp = strlen("*");
    HAL_UART_Transmit(&huart1_t, ReqTimestamp, LenofReqTimestamp, 100);
    return RE_OK;
}

/**
  * @Brief RE_Transmit_IdsToEc20
  * This function transmits battery Physical and CAN Id's to EC20
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_Tx_Bat_ID_EC20(void)
{
    uint8_t Ec20_TxBuffer[100];
    uint16_t LenofEc20_TxBuffer;
    uint8_t buffer[30];    
    memset(Ec20_TxBuffer, 0, sizeof(Ec20_TxBuffer));   
    strcat((char *)Ec20_TxBuffer, "15");
    sprintf((char *)buffer, "%s%s,%d", "#",BatInfo[0].PhyId, BatInfo[0].CanId);
    strcat((char *)Ec20_TxBuffer, (char *)buffer);
    memset(buffer, 0, sizeof(buffer));
    sprintf((char *)buffer, "%s%s,%d", "#",BatInfo[1].PhyId, BatInfo[1].CanId);
    strcat((char *)Ec20_TxBuffer, (char *)buffer);
    memset(buffer, 0, sizeof(buffer));
    sprintf((char *)buffer, "%s%s,%d", "#",BatInfo[2].PhyId, BatInfo[2].CanId);
    strcat((char *)Ec20_TxBuffer, (char *)buffer);
    memset(buffer, 0, sizeof(buffer));
    sprintf((char *)buffer, "%s%s,%d", "#",BatInfo[3].PhyId, BatInfo[3].CanId);
    strcat((char *)Ec20_TxBuffer, (char *)buffer);   
    strcat((char *)Ec20_TxBuffer, "\n");
    LenofEc20_TxBuffer = strlen((char *)Ec20_TxBuffer);
    HAL_UART_Transmit(&huart1_t, Ec20_TxBuffer,  LenofEc20_TxBuffer, 200);
    return RE_OK;
}

RE_StatusTypeDef RE_Req_SwapData(void)
{
    uint8_t ReqSwapData[] = "18\n";
    uint16_t LenofReqSwapData = strlen("18");
    HAL_UART_Transmit(&huart1_t, ReqSwapData, LenofReqSwapData, 100);    
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/