/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   main.c
  * Origin Date           :   13/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#include "main/main.h"
#include "fatfs.h"

uint8_t key_status = 0;
bool pack_id_req_pending = true;
bool update_bat_id = true;
bool ec20_lcd_data_rx = true;
uint8_t ec20_lcd_data_rx_fail_cnt = 0;

SD_HandleTypeDef hsd;
DMA_HandleTypeDef hdma_sdio_rx;
DMA_HandleTypeDef hdma_sdio_tx;
extern char SDPath[4];            /* SD logical drive path */
extern FATFS SDFatFS;             /* File system object for SD logical drive */
extern FIL SDFile;                /* File object for SD */
FATFS *pfs;
FRESULT fres;
DWORD fre_clust;
uint32_t totalSpace, freeSpace;
uint32_t byteswritten, bytesread; /* File write/read counts */
uint8_t rtext[100];
char allPackInfoBuffer[600];

static void MX_SDIO_SD_Init(void);
static void MX_DMA_Init(void);
static void clearbuf(void);
static void sd_mount(void);
static RE_StatusTypeDef RE_Load_System_Status(void);

/**
  * @Brief  Load system status.
  * @Retval RE_OK
  */
static RE_StatusTypeDef RE_Load_System_Status(void)
{
    RE_Send_System_Start_Cmd();
    RE_Load_Gear_Pos();    
    /** @TODO: request ODO from FRIGG */
    return RE_OK;
}

/**
  * @Brief  The application entry point.
  * @Retval int
  */
int main(void)
{
    HAL_Init();
    RE_SystemClock_Config();
    RE_RTC_Init();
    RE_SetTimeStamp();
    RE_LCD_Init();
    RE_Ec20_Init();
    RE_CAN1_Init();
    RE_CAN1_Filter_Config();
    RE_CAN1_Start_Interrupt();
    RE_CAN2_Init();
    RE_CAN2_Filter_Config();
    RE_CAN2_Start_Interrupt();
    RE_UART1_Init();
    RE_UART2_Init();
    RE_UART3_Init();
    Ringbuf_Init();
    RE_I2C1_Init();
    RE_SPI_Init();
    RE_TIMER2_Init();
    RE_TIMER3_Init();
    RE_TIMER5_Init();  
    MX_DMA_Init();
    MX_SDIO_SD_Init();
    MX_FATFS_Init();
    sd_mount();
    RE_Gear_Pos_Init();
    RE_Load_System_Status();
    HAL_Delay(100);
//    RE_Req_BatIds();
    if(HAL_TIM_Base_Start_IT(&htim5_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    } 
    while (1)
    {
        RE_Idle_State_Handler();
    }
}

/**
  * @brief SDIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_SDIO_SD_Init(void)
{
    hsd.Instance                 = SDIO;
    hsd.Init.ClockEdge           = SDIO_CLOCK_EDGE_RISING;
    hsd.Init.ClockBypass         = SDIO_CLOCK_BYPASS_DISABLE;
    hsd.Init.ClockPowerSave      = SDIO_CLOCK_POWER_SAVE_DISABLE;
    hsd.Init.BusWide             = SDIO_BUS_WIDE_1B;
    hsd.Init.HardwareFlowControl = SDIO_HARDWARE_FLOW_CONTROL_DISABLE;
    hsd.Init.ClockDiv            = 0;
}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{
    __HAL_RCC_DMA2_CLK_ENABLE();
    /* DMA interrupt init */
    /* DMA2_Stream3_IRQn interrupt configuration */    
    HAL_NVIC_SetPriority(DMA2_Stream3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream3_IRQn);
    /* DMA2_Stream6_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA2_Stream6_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream6_IRQn);
}

static void clearbuf(void)
{
    for (int i = 0; i < 600; i++)
    {
        allPackInfoBuffer[i] = '\0';
    }
}

/* Mount SD Card */
static void sd_mount(void)
{
    if(f_mount(&SDFatFS, (TCHAR const *)SDPath, 0) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /************ Card capacity details ************/
    /* Check free space */
    if (f_getfree("", &fre_clust, &pfs) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    totalSpace = (uint32_t)((pfs->n_fatent - 2) * pfs->csize * 0.5);
    sprintf(allPackInfoBuffer, "SD CARD Total Size: \t%lu\n", totalSpace);
    clearbuf();
    freeSpace = (uint32_t)(fre_clust * pfs->csize * 0.5);
    sprintf(allPackInfoBuffer, "SD Card Free Space: \t%1u\n", freeSpace);
    clearbuf();
    if (f_open(&SDFile, "ODO.txt", FA_OPEN_ALWAYS | FA_WRITE) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    
}

/** Log @pData into @pFile_name */
void AppendLog(char *pFileName, char *pData)
{
    if(f_open(&SDFile, pFileName, FA_OPEN_APPEND | FA_READ | FA_WRITE) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    f_puts(pData, &SDFile);
    if(f_close(&SDFile) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    clearbuf();
}

void OverWriteLog(char *pFileName, char *pData)
{
    if (f_open(&SDFile, pFileName, FA_OPEN_ALWAYS | FA_CREATE_ALWAYS | FA_WRITE) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /* Write data to text file */
    fres = f_write(&SDFile, pData, strlen((char *)pData), (void *)&byteswritten);
    if ((byteswritten == 0) || (fres != FR_OK))
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    f_close(&SDFile);    
}

uint32_t ReadLogFile(char *pFileName, uint8_t len)
{
    int Data = 0;
    /* Open file to read */
    fres = f_open(&SDFile, pFileName, FA_READ);
    memset(rtext, 0, sizeof(rtext));
    fres = f_read(&SDFile, rtext, sizeof(rtext), (UINT *)&bytesread);
    if ((bytesread == 0) || (fres != FR_OK))
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    else
    {
        Data = atoi((const char *)rtext);
    }
    f_close(&SDFile);
    return Data;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/