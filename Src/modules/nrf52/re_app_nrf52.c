/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_nrf52.c
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "nrf52/re_app_nrf52.h"

SwapInfo_t InitialSwapInfo[4], FinalSwapInfo[4];
bool G2G;
bool SwapModeFlag = false;
bool packsIdVerified = false;
bool VerifyPacks;
bool WriteData_To_Nvs_Flag;
bool write_g2g_to_nvs_flag;

/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/