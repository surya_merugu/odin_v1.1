/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   stm32f4xx_msp_init.c
  * Origin Date           :   13/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#include "re_hal_msp.h"

/**
  * Initializes the Global MSP.
  */
void HAL_MspInit(void)
{
    __HAL_RCC_SYSCFG_CLK_ENABLE();
    __HAL_RCC_PWR_CLK_ENABLE();
}

/**
  * @Brief SD MSP Initialization
  * This function configures the hardware resources used in this example
  * @Param hsd: SD handle pointer
  * @Retval None
  */
void HAL_SD_MspInit(SD_HandleTypeDef* hsd)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    if(hsd->Instance == SDIO)
    {
        __HAL_RCC_SDIO_CLK_ENABLE();
        __HAL_RCC_GPIOB_CLK_ENABLE();
        __HAL_RCC_GPIOC_CLK_ENABLE();
        __HAL_RCC_GPIOD_CLK_ENABLE();
        /**SDIO GPIO Configuration
        PB0     ------> SDIO_D1
        PB1     ------> SDIO_D2
        PB2     ------> SDIO_CK
        PC8     ------> SDIO_D0
        PC11    ------> SDIO_D3
        PD2     ------> SDIO_CMD
        */
        GPIO_InitStruct.Pin        = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2;
        GPIO_InitStruct.Mode       = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull       = GPIO_NOPULL;
        GPIO_InitStruct.Speed      = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate  = GPIO_AF12_SDIO;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

        GPIO_InitStruct.Pin        = GPIO_PIN_8 | GPIO_PIN_11;
        GPIO_InitStruct.Mode       = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull       = GPIO_NOPULL;
        GPIO_InitStruct.Speed      = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate  = GPIO_AF12_SDIO;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

        GPIO_InitStruct.Pin        = GPIO_PIN_2;
        GPIO_InitStruct.Mode       = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull       = GPIO_NOPULL;
        GPIO_InitStruct.Speed      = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate  = GPIO_AF12_SDIO;
        HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
        /* SDIO DMA Init */
        /* SDIO_RX Init */
        hdma_sdio_rx.Instance                  = DMA2_Stream3;
        hdma_sdio_rx.Init.Channel              = DMA_CHANNEL_4;
        hdma_sdio_rx.Init.Direction            = DMA_PERIPH_TO_MEMORY;
        hdma_sdio_rx.Init.PeriphInc            = DMA_PINC_DISABLE;
        hdma_sdio_rx.Init.MemInc               = DMA_MINC_ENABLE;
        hdma_sdio_rx.Init.PeriphDataAlignment  = DMA_PDATAALIGN_WORD;
        hdma_sdio_rx.Init.MemDataAlignment     = DMA_MDATAALIGN_WORD;
        hdma_sdio_rx.Init.Mode                 = DMA_PFCTRL;
        hdma_sdio_rx.Init.Priority             = DMA_PRIORITY_LOW;
        hdma_sdio_rx.Init.FIFOMode             = DMA_FIFOMODE_ENABLE;
        hdma_sdio_rx.Init.FIFOThreshold        = DMA_FIFO_THRESHOLD_FULL;
        hdma_sdio_rx.Init.MemBurst             = DMA_MBURST_INC4;
        hdma_sdio_rx.Init.PeriphBurst          = DMA_PBURST_INC4;
        if (HAL_DMA_Init(&hdma_sdio_rx) != HAL_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        __HAL_LINKDMA(hsd,hdmarx,hdma_sdio_rx);
        /* SDIO_TX Init */
        hdma_sdio_tx.Instance                  = DMA2_Stream6;
        hdma_sdio_tx.Init.Channel              = DMA_CHANNEL_4;
        hdma_sdio_tx.Init.Direction            = DMA_MEMORY_TO_PERIPH;
        hdma_sdio_tx.Init.PeriphInc            = DMA_PINC_DISABLE;
        hdma_sdio_tx.Init.MemInc               = DMA_MINC_ENABLE;
        hdma_sdio_tx.Init.PeriphDataAlignment  = DMA_PDATAALIGN_WORD;
        hdma_sdio_tx.Init.MemDataAlignment     = DMA_MDATAALIGN_WORD;
        hdma_sdio_tx.Init.Mode                 = DMA_PFCTRL;
        hdma_sdio_tx.Init.Priority             = DMA_PRIORITY_LOW;
        hdma_sdio_tx.Init.FIFOMode             = DMA_FIFOMODE_ENABLE;
        hdma_sdio_tx.Init.FIFOThreshold        = DMA_FIFO_THRESHOLD_FULL;
        hdma_sdio_tx.Init.MemBurst             = DMA_MBURST_INC4;
        hdma_sdio_tx.Init.PeriphBurst          = DMA_PBURST_INC4;
        if (HAL_DMA_Init(&hdma_sdio_tx) != HAL_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        __HAL_LINKDMA(hsd,hdmatx,hdma_sdio_tx);
        /* SDIO interrupt Init */
        HAL_NVIC_SetPriority(SDIO_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(SDIO_IRQn);
    }
}

/**
  * @Brief SD MSP De-Initialization
  * This function freeze the hardware resources used in this example
  * @Param hsd: SD handle pointer
  * @Retval None
  */
void HAL_SD_MspDeInit(SD_HandleTypeDef* hsd)
{
    if(hsd->Instance == SDIO)
    {
        __HAL_RCC_SDIO_CLK_DISABLE();
        /**SDIO GPIO Configuration
        PB0     ------> SDIO_D1
        PB1     ------> SDIO_D2
        PB2     ------> SDIO_CK
        PC8     ------> SDIO_D0
        PC11     ------> SDIO_D3
        PD2     ------> SDIO_CMD
        */
        HAL_GPIO_DeInit(GPIOB, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2);
        HAL_GPIO_DeInit(GPIOC, GPIO_PIN_8 | GPIO_PIN_11);
        HAL_GPIO_DeInit(GPIOD, GPIO_PIN_2);
        /* SDIO DMA DeInit */
        HAL_DMA_DeInit(hsd->hdmarx);
        HAL_DMA_DeInit(hsd->hdmatx);
        /* SDIO interrupt DeInit */
        HAL_NVIC_DisableIRQ(SDIO_IRQn);
    }
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/