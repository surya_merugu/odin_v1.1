/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_rtc_init.c
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_rtc_init.h"

RTC_HandleTypeDef hRTC_t;
RTC_TimeTypeDef sTime_t;
RTC_DateTypeDef sDate_t;

/**
  * @Brief RE_RTC_Init
  * This function configures the RTC peripheral
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_RTC_Init(void)
{
    __HAL_RCC_RTC_ENABLE();
    hRTC_t.Instance              = RTC;
    hRTC_t.Init.HourFormat       = RTC_HOURFORMAT_24;
    hRTC_t.Init.AsynchPrediv     = 127;
    hRTC_t.Init.SynchPrediv      = 255;
    hRTC_t.Init.OutPut           = RTC_OUTPUT_DISABLE;
    hRTC_t.Init.OutPutPolarity   = RTC_OUTPUT_POLARITY_HIGH;
    hRTC_t.Init.OutPutType       = RTC_OUTPUT_TYPE_OPENDRAIN;
    if (HAL_RTC_Init(&hRTC_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
  * @Brief RE_SetTimeStamp
  * This function initialise the timestamp of RTC peripheral
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_SetTimeStamp(void)
{
    sTime_t.Hours              = 0;
    sTime_t.Minutes            = 0;
    sTime_t.Seconds            = 1;
    sTime_t.DayLightSaving     = RTC_DAYLIGHTSAVING_NONE;
    sTime_t.StoreOperation     = RTC_STOREOPERATION_RESET;
    if (HAL_RTC_SetTime(&hRTC_t, &sTime_t, RTC_FORMAT_BIN) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    sDate_t.WeekDay       = RTC_WEEKDAY_WEDNESDAY;
    sDate_t.Month         = RTC_MONTH_JANUARY;
    sDate_t.Date          = 1;
    sDate_t.Year          = 20;
    if (HAL_RTC_SetDate(&hRTC_t, &sDate_t, RTC_FORMAT_BIN) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
  * @Brief RE_GetTimeStamp
  * This function gets the timestamp of RTC
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_GetTimeStamp(void)
{
    HAL_RTC_GetTime(&hRTC_t, &sTime_t, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(&hRTC_t, &sDate_t, RTC_FORMAT_BIN);
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/