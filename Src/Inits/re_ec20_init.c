/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_ec20_init.c
  * Origin Date           :   17/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_ec20_init.h"

/**
  * @Brief RE_Ec20_Init
  * This function initialises GPIO's used by EC20
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_Ec20_Init(void)
{
    GPIO_InitTypeDef enable, reset, wakeup, wakeup_disable, ap_ready, vbat_reg;
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    /*
    * PC6  : Enable
    * PC7  : reset
    * PA8  : wakeup
    * PC10 : wakeup diable
    * PC12 : Ap ready
    * PA15 : vbat_reg
    */
    enable.Pin     = GPIO_PIN_6; 
    enable.Mode    = GPIO_MODE_OUTPUT_PP;
    enable.Pull    = GPIO_PULLDOWN;
    enable.Speed   = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOC, &enable);

    reset.Pin     = GPIO_PIN_7; 
    reset.Mode    = GPIO_MODE_OUTPUT_PP;
    reset.Pull    = GPIO_PULLDOWN;
    reset.Speed   = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOC, &reset);

    wakeup.Pin     = GPIO_PIN_8; 
    wakeup.Mode    = GPIO_MODE_OUTPUT_PP;
    wakeup.Pull    = GPIO_PULLDOWN;
    wakeup.Speed   = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOA, &wakeup);

    wakeup_disable.Pin    = GPIO_PIN_10; 
    wakeup_disable.Mode   = GPIO_MODE_OUTPUT_PP;
    wakeup_disable.Pull   = GPIO_PULLDOWN;
    wakeup_disable.Speed  = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOC, &wakeup_disable);

    ap_ready.Pin    = GPIO_PIN_12; 
    ap_ready.Mode   = GPIO_MODE_OUTPUT_PP;
    ap_ready.Pull   = GPIO_PULLDOWN;
    ap_ready.Speed  = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOC, &ap_ready);
    
    vbat_reg.Pin    = GPIO_PIN_15; 
    vbat_reg.Mode   = GPIO_MODE_OUTPUT_PP;
    vbat_reg.Pull   = GPIO_PULLDOWN;
    vbat_reg.Speed  = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOA, &vbat_reg);   
 
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);     /* Enabling EC20 */
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/