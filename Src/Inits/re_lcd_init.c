/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_lcd_init.c
  * Origin Date           :   13/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   FSTN LCD
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_lcd_init.h"

GPIO_InitTypeDef cs1_pin, cs2_pin, clk_pin, data_pin, lcd_ctrl;

/**
  * @Brief RE_LCD_Init
  * This function iniialises the GPIO pins used by Dashboard LCD
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_LCD_Init(void)
{
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /*
    * PB8  : cs1_pin
    * PB9  : cs2_pin
    * PC13 : clk_pin
    * PC14 : data_pin
    * PB14 : Lcd_Ctrl
    */
    cs1_pin.Pin     = GPIO_PIN_8; 
    cs1_pin.Mode    = GPIO_MODE_OUTPUT_PP;
    cs1_pin.Pull    = GPIO_PULLDOWN;
    cs1_pin.Speed   = GPIO_SPEED_LOW;
    HAL_GPIO_Init(GPIOB, &cs1_pin);

    cs2_pin.Pin     = GPIO_PIN_9; 
    cs2_pin.Mode    = GPIO_MODE_OUTPUT_PP;
    cs2_pin.Pull    = GPIO_PULLDOWN;
    cs2_pin.Speed   = GPIO_SPEED_LOW;
    HAL_GPIO_Init(GPIOB, &cs2_pin);

    clk_pin.Pin     = GPIO_PIN_13; 
    clk_pin.Mode    = GPIO_MODE_OUTPUT_PP;
    clk_pin.Pull    = GPIO_PULLDOWN;
    clk_pin.Speed   = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOC, &clk_pin);

    data_pin.Pin    = GPIO_PIN_14; 
    data_pin.Mode   = GPIO_MODE_OUTPUT_PP;
    data_pin.Pull   = GPIO_PULLDOWN;
    data_pin.Speed  = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOC, &data_pin);

    lcd_ctrl.Pin    = GPIO_PIN_14; 
    lcd_ctrl.Mode   = GPIO_MODE_OUTPUT_PP;
    lcd_ctrl.Pull   = GPIO_NOPULL;
    lcd_ctrl.Speed  = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &lcd_ctrl);
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/