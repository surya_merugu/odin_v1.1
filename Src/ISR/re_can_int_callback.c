/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_can_int_callback.c
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */

#include "re_can_int_callback.h"

uint8_t battery_num = 0;

uint8_t animate;
uint8_t battery_count;
uint8_t onePackInfoBuffer[150];
bool onePackInfoBuffEmpty = true;
bool dockLatchClosed = false;
bool TransmitLatchStatus_Flag;
uint8_t Backup_BatVoltage;

/**
  ==============================================================================
                          ##### Callback functions #####
  ==============================================================================
    [..]
    This subsection provides the following callback functions:
      ( ) HAL_CAN_TxMailbox0CompleteCallback
      ( ) HAL_CAN_TxMailbox1CompleteCallback
      ( ) HAL_CAN_TxMailbox2CompleteCallback
      ( ) HAL_CAN_TxMailbox0AbortCallback
      ( ) HAL_CAN_TxMailbox1AbortCallback
      ( ) HAL_CAN_TxMailbox2AbortCallback
      (+) HAL_CAN_RxFifo0MsgPendingCallback
      ( ) HAL_CAN_RxFifo0FullCallback
      ( ) HAL_CAN_RxFifo1MsgPendingCallback
      ( ) HAL_CAN_RxFifo1FullCallback
      ( ) HAL_CAN_SleepCallback
      ( ) HAL_CAN_WakeUpFromRxMsgCallback
      ( ) HAL_CAN_ErrorCallback
  ==============================================================================
*/

/**
  * @Brief HAL_CAN_RxFifo0MsgPendingCallback
  * This function handles the messages in CAN FIFO0
  * @Param hcan: CAN_HandleTypeDef
  * @Retval None
  */
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
    uint8_t rx_msg[8];
    uint8_t strBuffer[50];
    if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &CAN1_RxHeader_t, rx_msg) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    
    /** FRIGG DATA */
    if(CAN1_RxHeader_t.ExtId == 0x7D0) 
    {
        switch(rx_msg[0])
        {
            case 0x01: /* On Key State change */
                /** [0]: msg ID 
                    [1]: KeyStatus
                    [2]: LatchStatus
                    [3]: Motor Fault
                    [4:7]: ODO (HEX)
                    **/
                key_status = rx_msg[1];
                LcdDisplay.lock_status = rx_msg[2];
                LcdDisplay.motor_fault_graph = rx_msg[3];
                LcdDisplay.odo = (rx_msg[4]<< 24 | rx_msg[5]<< 16 | rx_msg[6]<< 8 | rx_msg[7]);
                animate = key_status;
                break;
  
            case 0x02:
                if((LcdDisplay.lock_status != rx_msg[2])&& (SwapModeFlag == true))
                {
                    LcdDisplay.lock_status = rx_msg[2];
                    TransmitLatchStatus_Flag = true;               
                }
                LcdDisplay.speed          = rx_msg[1];
                /** 0 : CLOSE; 1: OPEN */
                LcdDisplay.lock_status = rx_msg[2];
                /**0 : Fuse Cutoff;  1 : No Cutoff  */ 
                LcdDisplay.motor_fault_graph = rx_msg[3];
                Backup_BatVoltage = rx_msg[4];
                break;
                      
            case 0x03:
                /** [0]: msg ID
                [1:4]: ODO
                **/     
                LcdDisplay.odo = (rx_msg[1]<< 24 | rx_msg[2]<< 16 | rx_msg[3]<< 8 | rx_msg[4]);
                break;
                
            case 0x04:
                /** 0 : CLOSE; 1: OPEN */
                LcdDisplay.lock_status = rx_msg[1];
                battery_count = rx_msg[2];
                /**0 : Fuse Cutoff;  1 : No Cutoff  */ 
                LcdDisplay.motor_fault_graph = rx_msg[3];
                
                break; 
            default:
                break;
        }
    }
    /** HEIMDAL DATA */
    else 
    {
        if (rx_msg[0] == 0x02){
            BatInfo[battery_num].CanId = CAN1_RxHeader_t.ExtId;
            memcpy(BatInfo[battery_num].PhyId, rx_msg + 1, 7);
            BatInfo[battery_num].PhyId[7] = '\0';
            if(battery_num >= 3)
            {
                /** @TODO: Start pack verification on latch close */ 
                /** @TODO: Start timers on latch close */
                battery_num = 0;
            }
            battery_num++;
        }
        else
        {
            sprintf((char *)strBuffer, "#%d,%x,%x,%x,%x", CAN1_RxHeader_t.ExtId, rx_msg[0], ((rx_msg[2]<<8)|(rx_msg[1])),\
                     ((rx_msg[4]<<8)|(rx_msg[3])), ((rx_msg[6]<<8)|(rx_msg[5])));
            strcat((char *)allPackInfoBuffer, (char *)strBuffer);
            strcat((char *)onePackInfoBuffer, (char *)strBuffer);
            onePackInfoBuffEmpty = false;
        }
    }
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/