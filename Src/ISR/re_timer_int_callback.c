/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_timer_init_callback.c
  * Origin Date           :   17/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_timer_int_callback.h"

bool t2000ms_Flag = false;
bool t50ms_Flag   = false;
bool t500ms_Flag  = false;

/**
  * @Brief HAL_TIM_PeriodElapsedCallback
  * This function handles Timer interrupts
  * @Param htim: TIM_HandleTypeDef
  * @Retval None
  */
void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim)
{ 
    if(htim -> Instance == TIM2)
    {
        t50ms_Flag = true;
        if(bat_num >= 4)
        {      
            if(HAL_TIM_Base_Stop_IT(&htim2_t) != HAL_OK)
            {
                RE_Error_Handler(__FILE__, __LINE__);
            }
        }
    }
    if(htim -> Instance == TIM3)
    {
        t2000ms_Flag = true;
    }
    if(htim -> Instance == TIM5)
    {
        t500ms_Flag = true;
    }
    else 
    {
        __NOP();
    }
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/