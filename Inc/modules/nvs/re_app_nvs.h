/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_nvs.h
  * Origin Date           :   06/10/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, OCT 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_APP_NVS_H
#define _RE_APP_NVS_H

#include "stm32f4xx_hal.h"
#include "re_std_def.h"
#include "stm32f4xx_it.h"
#include "re_i2c_init.h"
#include "nrf52/re_app_nrf52.h"
#include "uart/re_app_UART_Ring.h"

RE_StatusTypeDef RE_Write_Struct_To_NVS(uint16_t MemAddr, uint8_t bat_num);
RE_StatusTypeDef RE_Read_Struct_From_NVS(uint16_t MemAddr, uint8_t bat_num);
RE_StatusTypeDef RE_WriteBytetoNVS(uint16_t MemAddr, uint8_t Data);
RE_StatusTypeDef RE_ReadByteFromNVS(uint16_t MemAddr);
RE_StatusTypeDef RE_Write_DwordToNVS(uint16_t MemAddr, uint32_t Data);
RE_StatusTypeDef RE_Read_DwordFromNVS(uint16_t MemAddr);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/