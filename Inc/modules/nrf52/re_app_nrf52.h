/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_nrf52.h
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_APP_NRF52_H
#define _RE_APP_NRF52_H

#include "stdbool.h"
#include "stdint.h"
#include "re_std_def.h"
#include "can/re_app_can.h"
#include "main/main.h"
#include "re_rtc_init.h"
#include "nvs/re_app_nvs.h"

typedef struct
{
    uint8_t pid[8];
    uint16_t Energy;
    uint8_t Alert;
}SwapInfo_t;

extern SwapInfo_t InitialSwapInfo[4], FinalSwapInfo[4];
extern bool G2G;
extern bool SwapModeFlag;
extern bool packsIdVerified;
extern bool VerifyPacks;
extern bool WriteData_To_Nvs_Flag;
extern bool write_g2g_to_nvs_flag;

#endif
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/