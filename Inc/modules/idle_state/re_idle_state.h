/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_idle_state.h
  * Origin Date           :   17/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#ifndef __RE_IDLE_STATE_H
#define __RE_IDLE_STATE_H

#include "re_std_def.h"
#include "re_can_int_callback.h"
#include "re_can_init.h"
#include "nrf52/re_app_nrf52.h"
#include "can/re_app_can.h"
#include "re_can_int_callback.h"
#include "re_timer_int_callback.h"
#include "re_rtc_init.h"
#include "dash_lcd/re_app_dash_lcd.h"
#include "dash_lcd/re_driver_dash_lcd.h"
#include "ec20/re_app_ec20.h"
#include "uart/re_app_UART_Ring.h"

extern uint8_t bat_num;
extern uint8_t retryVerifyCnt;
extern uint8_t key_status;
extern bool pack_id_req_pending;
extern bool ec20_lcd_data_rx;
extern uint8_t ec20_lcd_data_rx_fail_cnt;
extern bool update_bat_id;
RE_StatusTypeDef RE_Idle_State_Handler(void);

#endif
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/