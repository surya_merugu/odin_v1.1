/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_ec20.h
  * Origin Date           :   17/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_APP_EC20_H
#define _RE_APP_EC20_H

#include "stdbool.h"
#include "stdint.h"
#include "string.h"
#include "re_uart_init.h"
#include "re_ec20_init.h"
#include "can/re_app_can.h"
#include "stdio.h"

extern uint8_t EC20_Req_Timestamp_Counter;
extern bool WaitForEC20Timestamp;
extern bool EnableEC20;
extern bool Tx_IdsToEc20_Flag;
extern bool Req_SwapData_From_EC20_Flag;

RE_StatusTypeDef RE_ReqEC20_Timestamp(void);
RE_StatusTypeDef RE_Tx_Bat_ID_EC20(void);
RE_StatusTypeDef RE_Req_SwapData(void);

#endif
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/