/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_gear_pos_init.h
  * Origin Date           :   03/09/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, SEP 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_GEAR_POS_INIT_H
#define _RE_GEAR_POS_INIT_H

/* Includes */
#include "re_std_def.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_it.h"
#include "can/re_app_can.h"

/* Exported API */
RE_StatusTypeDef RE_Gear_Pos_Init (void);
RE_StatusTypeDef RE_Load_Gear_Pos (void);

#endif
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/