/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_hal_msp.h
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_HAL_MSP_H
#define _RE_HAL_MSP_H

#include "main/main.h"

void HAL_MspInit(void);

#endif
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/