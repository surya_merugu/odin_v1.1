/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_rtc_init.h
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_RTC_INIT_H
#define _RE_RTC_INIT_H

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_rtc.h"
#include "re_std_def.h"

extern RTC_HandleTypeDef hRTC_t;
extern RTC_TimeTypeDef sTime_t;
extern RTC_DateTypeDef sDate_t;

RE_StatusTypeDef RE_RTC_Init(void);
RE_StatusTypeDef RE_GetTimeStamp(void);
RE_StatusTypeDef RE_SetTimeStamp(void);

#endif
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/